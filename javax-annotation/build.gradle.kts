import name.remal.fromLowerHyphenToLowerCamel
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension
import name.remal.version.Version
import java.util.SortedMap

configure<MergeArtifacts> {
    automaticModuleName = "java.annotation"
}


val dependencyVersions = get(DependencyVersionsExtension::class.java)
val publications = get(PublishingExtension::class.java).publications

with(dependencyVersions) {
    allowAllVersionsFor("javax.annotation:javax.annotation-api")
    allowAllVersionsFor("jakarta.annotation:jakarta.annotation-api")
    rejectVersions("jakarta.annotation:jakarta.annotation-api:2+")
}
val mainVersionsRedirect = mapOf<Version, Version>(
)
val mainVersions: SortedMap<Version, DependencyNotation> = sortedMapOf<Version, DependencyNotation>().apply {
    arrayOf(
        DependencyNotation("javax.annotation", "javax.annotation-api"),
        DependencyNotation("jakarta.annotation", "jakarta.annotation-api")
    ).forEach { notation ->
        dependencyVersions.resolveAllVersions(notation)
            .mapNotNull(Version::parseOrNull)
            .forEach { version ->
                get(version)?.let { throw GradleException("Main version $version has been already mapped to $it") }
                val canonicalVersion = mainVersionsRedirect[version] ?: version
                val versionNotation = notation.withVersion(canonicalVersion.toString())
                if (getDependencyVersionDateOrNull(versionNotation) != null) {
                    put(version, versionNotation)
                }
            }
    }
}
with(dependencyVersions) {
    allowAllVersionsFor("com.google.code.findbugs:jsr305")
}
val otherVersionsRedirect = mapOf<Version, Version>(
    Version.create(1, 3, 8) to Version.create(1, 3, 9) // version with broken POM
)
val otherVersions: SortedMap<Version, DependencyNotation> = sortedMapOf<Version, DependencyNotation>().apply {
    arrayOf(
        DependencyNotation("com.google.code.findbugs", "jsr305")
    ).forEach { notation ->
        dependencyVersions.resolveAllVersions(notation)
            .mapNotNull(Version::parseOrNull)
            .forEach { version ->
                get(version)?.let { throw GradleException("Other version $version has been already mapped to $it") }
                val canonicalVersion = otherVersionsRedirect[version] ?: version
                val versionNotation = notation.withVersion(canonicalVersion.toString())
                if (getDependencyVersionDateOrNull(versionNotation) != null) {
                    put(version, versionNotation)
                }
            }
    }
}


data class Versions(val main: Version, val other: Version?)

val mainVersionsMapping: SortedMap<Version, Versions> = sortedMapOf<Version, Versions>().apply {
    mainVersions.forEach { mainVersion, mainNotation ->
        val mainDate = getDependencyVersionDate(mainNotation)
        val otherVersion = otherVersions.entries.lastOrNull filter@{ (_, otherNotation) ->
            val otherDate = getDependencyVersionDate(otherNotation)
            return@filter otherDate <= mainDate
        }?.key
        put(mainVersion, Versions(mainVersion, otherVersion))
    }

    val mainVersion = lastKey()!!
    otherVersions.entries
        .filter { (_, otherNotation) ->
            val otherDate = getDependencyVersionDate(otherNotation)
            val hasCorrespondingMainVersion = mainVersions.values.any { mainNotation ->
                val mainDate = getDependencyVersionDate(mainNotation)
                return@any mainDate >= otherDate
            }
            return@filter !hasCorrespondingMainVersion
        }
        .forEachIndexed { index, (otherVersion, _) ->
            val artifactVersion = lastKey()!!.let { ver ->
                if (index == 0) {
                    ver.incrementNumber(ver.numbersCount + 1)
                } else {
                    ver.incrementNumber(ver.numbersCount - 1)
                }
            }
            put(artifactVersion, Versions(mainVersion, otherVersion))
        }
}

val mainArtifactName = project.name
mainVersionsMapping.forEach { artifactVersion, (mainVersion, otherVersion) ->
    val mainNotation = mainVersion?.let(mainVersions::get)
    val otherNotation = otherVersion?.let(otherVersions::get)
    val notations = listOfNotNull(mainNotation, otherNotation)

    publications.create<MavenPublication>(mainArtifactName.fromLowerHyphenToLowerCamel() + '-' + artifactVersion) publication@{
        artifactId = mainArtifactName
        version = artifactVersion.toString()

        pom {
            packaging = "jar"
            name.set(notations.joinToString(" + "))
        }

        mergedArtifact(
            project = project,
            notations = notations
        )

        mergedSourcesArtifact(
            project = project,
            notations = notations
        )
    }
}


val otherVersionsMapping: SortedMap<Version, Version> = sortedMapOf<Version, Version>().apply {
    otherVersions.forEach { otherVersion, otherNotation ->
        val artifactVersion = mainVersionsMapping.entries.firstOrNull { otherVersion == it.value.other }?.key
        if (artifactVersion != null) {
            put(otherVersion, artifactVersion)

        } else {
            val otherDate = getDependencyVersionDate(otherNotation)
            val mainVersion = mainVersions.entries.first filter@{ (_, mainNotation) ->
                val mainDate = getDependencyVersionDate(mainNotation)
                return@filter otherDate <= mainDate
            }.key
            put(otherVersion, mainVersion)
        }
    }
}

val otherArtifactName = project.name + "-findbugs"
otherVersionsMapping.forEach { otherVersion, artifactVersion ->
    val mainVersion = mainVersionsMapping[artifactVersion]!!.main
    val mainNotation = mainVersions[mainVersion]
    val otherNotation = otherVersions[otherVersion]
    val notations = listOfNotNull(mainNotation, otherNotation)

    publications.create<MavenPublication>(otherArtifactName.fromLowerHyphenToLowerCamel() + '-' + otherVersion) publication@{
        artifactId = otherArtifactName
        version = otherVersion.toString()

        pom {
            packaging = "pom"
            name.set(notations.joinToString(" + "))

            distributionManagement {
                relocation {
                    groupId.set(this@publication.groupId)
                    artifactId.set(mainArtifactName)
                    version.set(artifactVersion.toString())
                    this.message.set("Relocated to a canonical artifact")
                }
            }
        }
    }
}
