import name.remal.gradle_plugins.dsl.Extension
import org.jetbrains.kotlin.allopen.gradle.AllOpenExtension
import name.remal.gradle_plugins.dsl.Plugin as RemalPlugin

buildscript {
    dependencies {
        classpath(embeddedKotlin("gradle-plugin"))
        classpath(embeddedKotlin("allopen"))
    }
}

plugins {
    java
    `kotlin-dsl`
}

kotlinDslPluginOptions {
    experimentalWarning.set(false)
}

apply { from("../gradle/build.gradle.kts") }

configure<AllOpenExtension> {
    arrayOf(
        RemalPlugin::class.java,
        Extension::class.java
    ).forEach {
        annotation(it.name)
    }
}

dependencies {
    implementation(gradleKotlinDsl())
    implementation("name.remal:gradle-plugins")
    implementation("org.jsoup:jsoup")
}
