import name.remal.gradle_plugins.dsl.extensions.autoFileTree
import name.remal.gradle_plugins.dsl.extensions.createFromNotation
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.makeNotTransitive
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.bundling.Jar
import org.gradle.kotlin.dsl.create

fun MavenPublication.mergedArtifact(
    project: Project,
    notations: Iterable<DependencyNotation>,
    automaticModuleName: String? = null
) {
    artifact(project.tasks.create<Jar>("jar" + this@mergedArtifact.name.capitalize()) {
        archiveBaseName.set(this@mergedArtifact.version)
        archiveVersion.set("")

        if (automaticModuleName != null) {
            manifest.attributes["Automatic-Module-Name"] = automaticModuleName
        }

        doSetup {
            val configuration = project.configurations.detachedConfiguration(
                *notations.toSet().map(project.dependencies::createFromNotation).toTypedArray()
            ).makeNotTransitive()

            configuration.files.forEach {
                from(project.autoFileTree(it)) {
                    applyBaseExclusions()
                    exclude("**/*.java")
                }
            }
        }
    })
}

fun MavenPublication.mergedSourcesArtifact(
    project: Project,
    notations: Iterable<DependencyNotation>
) {
    artifact(project.tasks.create<Jar>("sourcesJar" + this@mergedSourcesArtifact.name.capitalize()) {
        archiveBaseName.set(this@mergedSourcesArtifact.version)
        archiveVersion.set("")
        archiveClassifier.set("sources")

        doSetup {
            val sourcesNotation = notations.flatMap { listOf(it, it.withClassifier("sources")) }
            val configuration = project.configurations.detachedConfiguration(
                *sourcesNotation.toSet().map(project.dependencies::createFromNotation).toTypedArray()
            ).makeNotTransitive()

            configuration.resolvedConfiguration.lenientConfiguration.files.forEach {
                from(project.autoFileTree(it)) {
                    applyBaseExclusions()
                    exclude("**/*.class")
                    exclude("META-INF/services/*.shaded.*")
                }
            }
        }
    })
}
