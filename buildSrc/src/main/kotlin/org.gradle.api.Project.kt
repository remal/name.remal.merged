import name.remal.concurrentMapOf
import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.jsoup.Jsoup
import java.io.File
import java.net.URL
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatterBuilder
import java.time.format.SignStyle.NOT_NEGATIVE
import java.time.temporal.ChronoField.*
import java.util.Locale.ENGLISH
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.collections.set
import kotlin.text.isNotEmpty
import kotlin.text.trim

fun Project.getCiProperty(name: String): String? {
    System.getenv(name.replace(Regex("\\W"), "_")).nullIfEmpty()?.let { return it }
    findProperty(name).unwrapProviders()?.toString().nullIfEmpty()?.let { return it }
    return null
}


fun Project.getDependencyVersionDate(notation: DependencyNotation): LocalDate {
    return getDependencyVersionDateOrNull(notation)
        ?: throw IllegalStateException("Dependency version date can't be defined: $notation")
}

fun Project.getDependencyVersionDateOrNull(notation: DependencyNotation): LocalDate? {
    if (notation.version.isEmpty()) return null

    synchronized(locks.computeIfAbsent(notation.withOnlyGroupAndModule(), { Any() })) {

        val foundCacheFile = rootDir.resolve("state/dependency-versions.cache")
        val notFoundCacheFile = rootDir.resolve("state/dependency-versions-not-found.cache")
        if (isCacheLoaded.compareAndSet(false, true)) {
            loadCache(foundCacheFile, notFoundCacheFile)
        }

        val canonicalNotation = notation.withoutClassifier().withoutExtension()
        foundCache[canonicalNotation]?.let { return it }
        if (canonicalNotation in notFoundCache) return null


        Thread.sleep(2500)

        val document = Jsoup.parse(URL("https://mvnrepository.com/artifact/${notation.group}/${notation.module}"), 10000)
        val versions = mutableMapOf<String, String>()
        document.select("table.versions>tbody>tr").forEach { tr ->
            val version = tr.select(".vbtn").text().trim().nullIfEmpty() ?: return@forEach
            val dateStr = tr.select("td").lastOrNull { it.text().contains(',') }?.text()?.trim().nullIfEmpty() ?: return@forEach
            versions[version] = dateStr
        }

        if (versions.isNotEmpty()) {
            versions.forEach { version, dateStr ->
                val versionNotation = canonicalNotation.withVersion(version)
                val date = try {
                    LocalDate.parse("1 $dateStr", dateFormat)
                } catch (e: Throwable) {
                    logger.warn("{}: date can't be parsed: {}", versionNotation, e)
                    null
                }
                if (date != null) {
                    foundCache[versionNotation] = date
                } else {
                    notFoundCache[versionNotation] = LocalDate.now()
                }
            }
            storeCache(foundCacheFile, notFoundCacheFile)

        } else {
            return null
        }

        if (canonicalNotation !in foundCache) {
            logger.warn("{}: date can't be found", canonicalNotation)
            notFoundCache[canonicalNotation] = LocalDate.now()
            storeCache(foundCacheFile, notFoundCacheFile)
            return null
        }

        return foundCache[canonicalNotation]
    }
}


private val locks: ConcurrentMap<DependencyNotation, Any> = concurrentMapOf()

private val isCacheLoaded = AtomicBoolean()
private val foundCache: ConcurrentMap<DependencyNotation, LocalDate> = concurrentMapOf()
private val notFoundCache: ConcurrentMap<DependencyNotation, LocalDate> = concurrentMapOf()

private val dateFormat = DateTimeFormatterBuilder()
    .appendValue(DAY_OF_MONTH, 1, 2, NOT_NEGATIVE)
    .appendLiteral(' ')
    .appendText(MONTH_OF_YEAR, mapOf(
        1L to "Jan",
        2L to "Feb",
        3L to "Mar",
        4L to "Apr",
        5L to "May",
        6L to "Jun",
        7L to "Jul",
        8L to "Aug",
        9L to "Sep",
        10L to "Oct",
        11L to "Nov",
        12L to "Dec"
    ))
    .appendLiteral(", ")
    .appendValue(YEAR, 4)
    .toFormatter(ENGLISH)

private fun loadCache(foundCacheFile: File, notFoundCacheFile: File) {
    if (foundCacheFile.isFile) {
        foundCacheFile.readLines().asSequence()
            .map(String::trim)
            .filter(String::isNotEmpty)
            .forEach { line ->
                val dateNotation = parseDependencyNotation(line)
                val notation = dateNotation.withoutClassifier()
                val dateStr = dateNotation.classifier
                val date = LocalDate.parse(dateStr)
                foundCache[notation] = date
            }
    }

    if (notFoundCacheFile.isFile) {
        val minDate = LocalDate.now() - Period.ofDays(7)
        notFoundCacheFile.readLines().asSequence()
            .map(String::trim)
            .filter(String::isNotEmpty)
            .forEach { line ->
                val dateNotation = parseDependencyNotation(line)
                val notation = dateNotation.withoutClassifier()
                val dateStr = dateNotation.classifier
                val date = LocalDate.parse(dateStr)
                if (date >= minDate) {
                    notFoundCache[notation] = date
                }
            }
    }
}

private fun storeCache(foundCacheFile: File, notFoundCacheFile: File) {
    foundCacheFile.createParentDirectories().writeText(
        foundCache.asSequence()
            .map { it.key.withClassifier(it.value.toString()).withoutExtension() }
            .sorted()
            .joinToString("\n") + "\n"
    )

    notFoundCacheFile.createParentDirectories().writeText(
        notFoundCache.asSequence()
            .map { it.key.withClassifier(it.value.toString()).withoutExtension() }
            .sorted()
            .joinToString("\n") + "\n"
    )
}
