import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import org.jdom2.Element
import org.jdom2.Namespace

fun Element.dependency(notation: DependencyNotation, namespace: Namespace? = document.rootElement.namespace) {
    addContent(Element("dependency", namespace).apply {
        addContent(Element("groupId", namespace).setText(notation.group))
        addContent(Element("artifactId", namespace).setText(notation.module))
        addContent(Element("version", namespace).setText(notation.version))
    })
}
