import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.Project
import javax.inject.Inject

@Extension
@MergeArtifactsDsl
class MergeArtifacts @Inject constructor(
    project: Project
) {

    var automaticModuleName: String? = project.name.replace(Regex("\\W"), ".")

}


@DslMarker
private annotation class MergeArtifactsDsl
