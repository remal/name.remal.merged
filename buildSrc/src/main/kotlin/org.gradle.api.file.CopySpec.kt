import org.gradle.api.file.CopySpec
import org.gradle.api.file.DuplicatesStrategy.EXCLUDE

fun CopySpec.applyBaseExclusions() {
    exclude("META-INF/*.MF")
    exclude("META-INF/*.SF")
    exclude("META-INF/*.DSA")
    exclude("META-INF/*.RSA")
    exclude("META-INF/LICENSE.*")
    exclude("META-INF/NOTICE.*")
    exclude("META-INF/maven/")
    exclude("pom.xml")
    exclude("**/package.html")

    duplicatesStrategy = EXCLUDE
}
