import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension

fun DependencyVersionsExtension.resolveLatestVersion(notation: DependencyNotation) = resolveLatestVersion(notation.toString())
fun DependencyVersionsExtension.resolveAllVersions(notation: DependencyNotation) = resolveAllVersions(notation.toString())

inline fun <R> DependencyVersionsExtension.withoutFilters(action: DependencyVersionsExtension.() -> R): R {
    val rejectVersionsPrev = rejectVersions.toSet()
    val allowAllVersionsForPrev = allowAllVersionsFor.toSet()
    val invalidVersionTokensPrev = invalidVersionTokens.toSet()
    rejectVersions = mutableSetOf()
    allowAllVersionsFor = mutableSetOf()
    invalidVersionTokens = mutableSetOf()

    try {
        return action(this)

    } finally {
        rejectVersions = rejectVersionsPrev.toMutableSet()
        allowAllVersionsFor = allowAllVersionsForPrev.toMutableSet()
        invalidVersionTokens = invalidVersionTokensPrev.toMutableSet()
    }
}
