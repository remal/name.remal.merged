import name.remal.*
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import org.jdom2.Document
import org.jdom2.input.SAXBuilder
import java.io.File

fun File.parsePomDocument(): Document {
    return saxBuilder.build(this)
}

fun File.parsePomDependencies(): PomDependencies {
    val document = parsePomDocument().clearNamespaces()

    return PomDependencies(
        packaging = sequenceOf(document.rootElement)
            .flatMap { it.getChildren("packaging").asSequence() }
            .map { it.textTrim }
            .filter(String::isNotEmpty)
            .firstOrNull()
            .default("jar"),

        direct = sequenceOf(document.rootElement)
            .flatMap { it.getChildren("dependencies").asSequence() }
            .flatMap { it.getChildren("dependency").asSequence() }
            .filter { it.getChild("scope")?.textTrim.nullIfEmpty().default("compile") == "compile" }
            .filter { it.getChild("optional")?.textTrim.nullIfEmpty()?.toBoolean() != true }
            .filter { it.getChild("systemPath")?.textTrim.nullIfEmpty() == null }
            .map { node ->
                return@map DependencyNotation(
                    group = node.getChild("groupId").textTrim,
                    module = node.getChild("artifactId").textTrim,
                    version = node.getChild("version").textTrim,
                    classifier = node.getChild("classifier")?.textTrim,
                    extension = node.getChild("type")?.textTrim
                )
            }
            .toSet(),

        managed = sequenceOf(document.rootElement)
            .flatMap { it.getChildren("dependencyManagement").asSequence() }
            .flatMap { it.getChildren("dependencies").asSequence() }
            .flatMap { it.getChildren("dependency").asSequence() }
            .filter { it.getChild("scope")?.textTrim.nullIfEmpty().default("compile") == "compile" }
            .filter { it.getChild("optional")?.textTrim.nullIfEmpty()?.toBoolean() != true }
            .filter { it.getChild("systemPath")?.textTrim.nullIfEmpty() == null }
            .map { node ->
                return@map DependencyNotation(
                    group = node.getChild("groupId").textTrim,
                    module = node.getChild("artifactId").textTrim,
                    version = node.getChild("version").textTrim,
                    classifier = node.getChild("classifier")?.textTrim,
                    extension = node.getChild("type")?.textTrim
                )
            }
            .toSet()
    )
}

data class PomDependencies(
    val packaging: String,
    val direct: Set<DependencyNotation>,
    val managed: Set<DependencyNotation>
)


private val saxBuilder = SAXBuilder().apply {
    setNoValidatingXMLReaderFactory()
    setNoOpEntityResolver()
}
