import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.*
import name.remal.gradle_plugins.dsl.extensions.*
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenArtifact
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.tasks.GenerateMavenPom
import org.gradle.api.publish.maven.tasks.PublishToMavenRepository
import org.gradle.api.publish.tasks.GenerateModuleMetadata
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.kotlin.dsl.withType
import java.io.File

@Plugin(id = "", description = "", tags = [])
class BuildSrcPlugin : BaseReflectiveProjectPlugin() {

    private var republish: Boolean = false

    @HighestPriorityPluginAction
    fun Project.updateRepublish() {
        if (getCiProperty("republish")?.toBoolean() == true) {
            logger.lifecycle("$path: 'republish' == true")
            republish = true

        } else if (getCiProperty("republish-$name")?.toBoolean() == true) {
            logger.lifecycle("$path: 'republish-$name' == true")
            republish = true
        }
    }


    @CreateExtensionsPluginAction
    fun Project.createSettingsExtensions() {
        createExtensionWithInjectedParams(MergeArtifacts::class.java)
    }


    @PluginAction
    fun clearDefaultArchives(configurations: ConfigurationContainer) {
        configurations.findByName("archives")?.artifacts?.clear()
    }


    @PluginAction
    fun TaskContainer.adjustAutomaticModuleName() {
        withType<Jar> {
            doSetup(-1) {
                if (archiveClassifier.orNull.isNullOrEmpty()) {
                    val moduleName = project.getOrNull(MergeArtifacts::class.java)?.automaticModuleName.nullIfEmpty()
                    if (moduleName != null) {
                        manifest.attributes.putIfAbsent("Automatic-Module-Name", moduleName)
                    }
                }
            }
        }
    }


    @PluginAction
    fun Project.skipBuildingAlreadyPublishedArtifacts(tasks: TaskContainer, extensions: ExtensionContainer) {
        if (republish) return
        withPlugin("maven-publish") { _ ->
            val publishing = extensions[PublishingExtension::class.java]
            setupTasksDependenciesAfterEvaluateOrNow { _ ->
                val mavenPublications = publishing.publications.filterIsInstance(MavenPublication::class.java)
                val mavenRepos = publishing.repositories.filterIsInstance(MavenArtifactRepository::class.java)
                tasks.forEach task@{ task ->
                    if (task.isRequested) return@task
                    mavenPublications.forEach publication@{ publication ->
                        val publicationFiles = publication.artifacts.map(MavenArtifact::getFile)
                        val taskFiles = task.outputs.files.toSet()
                        val buildPublicationFiles = publicationFiles.any { it in taskFiles }

                        val buildPublicationPom = task is GenerateMavenPom
                            && task.pom === publication.pom

                        val buildPublicationMetadata = task is GenerateModuleMetadata
                            && task.publication.orNull === publication

                        if (buildPublicationFiles || buildPublicationPom || buildPublicationMetadata) {
                            val isPublishedToAllRepos = mavenRepos.all { repo ->
                                getPublishStateFile(repo, publication).isFile
                            }
                            if (isPublishedToAllRepos) {
                                task.disableTask()
                            }
                        }
                    }
                }
            }
        }
    }

    @PluginAction
    fun TaskContainer.skipPublishingAlreadyPublishedArtifacts() {
        if (republish) return
        withType<PublishToMavenRepository> {
            val publishedFlagFile: File by lazy { project.getPublishStateFile(repository, publication) }
            onlyIf {
                if (publishedFlagFile.isFile) {
                    logger.lifecycle("{} has already been published to {}", publication.notation, repository.name)
                    return@onlyIf false
                }
                return@onlyIf true
            }
            doLast {
                publishedFlagFile.createParentDirectories().createNewFile()
            }
        }
    }


    private fun Project.getPublishStateFile(repository: MavenArtifactRepository, publication: MavenPublication): File {
        return getPublishStateFile(repository.name, publication)
    }

    private fun Project.getPublishStateFile(repositoryId: String, publication: MavenPublication): File {
        return rootDir.resolve(listOf(
            "state/published",
            repositoryId,
            publication.groupId,
            publication.artifactId,
            publication.version
        ).joinToString("/"))
    }

}
