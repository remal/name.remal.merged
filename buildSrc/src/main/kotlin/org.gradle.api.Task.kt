import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.uncheckedCast
import org.gradle.api.Task

fun Task.disableTask() {
    enabled = false
    setDependsOn(emptyList<Task>())

    val inputs = this.inputs
    inputs.javaClass.unwrapGradleGenerated()
        .getDeclaredField("registeredFileProperties")
        .apply { isAccessible = true }
        .get(inputs)
        .uncheckedCast<MutableIterable<Any>>()
        .iterator()
        .run {
            while (hasNext()) {
                next()
                remove()
            }
        }
}
