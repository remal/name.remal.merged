apply { from("../gradle/settings.gradle.kts") }

gradle.allprojects {
    configurations.all {
        dependencies.all {
            if (this is ModuleDependency) {
                exclude(group = "org.slf4j")
                exclude(group = "ch.qos.logback")
                exclude(group = "org.apache.logging.log4j")
                exclude(group = "log4j")
                exclude(group = "commons-logging")
                exclude(group = "org.springframework", module = "spring-jcl")
                exclude(group = "org.codehaus.groovy")
                exclude(group = "ant", module = "ant")
                exclude(group = "org.apache.ant", module = "ant")
                exclude(group = "org.apache.ant", module = "ant-launcher")
            }
        }
    }
}
