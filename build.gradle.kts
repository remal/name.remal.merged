import name.remal.gradle_plugins.dsl.extensions.*
import name.remal.gradle_plugins.integrations.gradle_org.GradleOrgClient
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.gradle_plugins.plugins.publish.bintray.RepositoryHandlerBintrayExtension
import name.remal.gradle_plugins.plugins.publish.ossrh.RepositoryHandlerOssrhExtension
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsExtension
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsPlugin
import org.gradle.api.JavaVersion.VERSION_12
import org.gradle.plugins.ide.idea.model.IdeaLanguageLevel
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.util.SingleMessageLogger
import org.jetbrains.gradle.ext.EncodingConfiguration
import org.jetbrains.gradle.ext.EncodingConfiguration.BomPolicy.WITH_NO_BOM
import org.jetbrains.gradle.ext.ProjectSettings
import java.lang.System.currentTimeMillis
import java.time.Duration

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

buildscript {
    dependencies {
        classpath("gradle.plugin.org.jetbrains.gradle.plugin.idea-ext:gradle-idea-ext")
    }
}

apply { from("gradle/build.gradle.kts") }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

subprojects {

    applyPlugin(JavaPluginId)
    applyPlugin(MavenPublishPluginId)
    applyPlugin(BuildSrcPlugin::class.java)

    configure<PublishingExtension> {
        publications.withType<MavenPublication> publication@{
            groupId = project.group.toString()

            pom {
                name.set(provider { this@publication.notation.toString() })
                description.set(provider { name.get() })

                url.set("https://gitlab.com/remal/name.remal.merged")
                scm {
                    url.set("https://gitlab.com/remal/name.remal.merged.git")
                }
                developers {
                    developer {
                        name.set("Semen Levin")
                        email.set("levin.semen@gmail.com")
                        id.set(email.get())
                    }
                }
                licenses {
                    license {
                        name.set("Unlicense")
                        url.set("https://unlicense.org/UNLICENSE")
                    }
                }
            }

            project.afterEvaluateOrNow {
                if (artifacts.isNotEmpty() && artifacts.none { it.classifier == "javadoc" }) {
                    artifact(tasks.create<Jar>("emptyJavadocJar" + this@publication.name.capitalize()) {
                        archiveBaseName.set(this@publication.version)
                        archiveVersion.set("")
                        archiveClassifier.set("javadoc")
                    })
                }
            }
        }

        repositories.convention[RepositoryHandlerBintrayExtension::class.java].bintray {
            owner = "remal"
            repositoryName = "name.remal"
            //packageName = "name.remal.merged"
            packageName = "name.remal.experimental"
        }

        repositories.convention[RepositoryHandlerOssrhExtension::class.java]//.ossrh()
    }

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

applyPlugin(VcsOperationsPlugin::class.java)

tasks.register("commitState") {
    doLast {
        project[VcsOperationsExtension::class.java].commitFiles(
            "[skip ci] state",
            project.files(
                "state"
            ).files
        )
        didWork = true
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

if (System.getenv("GITLAB_CI") != "true" && System.getenv("CI") != "true") {
    val gradleWrapperPropertiesFile = file("gradle/wrapper/gradle-wrapper.properties")
    if (gradleWrapperPropertiesFile.isFile) {
        if (gradleWrapperPropertiesFile.lastModified() <= currentTimeMillis() - Duration.ofDays(1).toMillis()) {
            val version = GradleOrgClient.getCurrentVersion().version
            gradleWrapperPropertiesFile.writeText(
                gradleWrapperPropertiesFile.readText()
                    .replace(Regex("(/gradle)-(?:\\d.*)-((all|bin)\\.zip)"), "\$1-$version-\$2")
            )
        }
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

SingleMessageLogger.whileDisabled {
    applyPlugin("org.jetbrains.gradle.plugin.idea-ext")
}

configure<IdeaModel> {
    project {
        jdkName = "12"
        languageLevel = IdeaLanguageLevel("12")
        targetBytecodeVersion = VERSION_12

        // see https://github.com/JetBrains/gradle-idea-ext-plugin/wiki/DSL-spec-v.-0.5
        (this as ExtensionAware).configure<ProjectSettings> {
            doNotDetectFrameworks("android")

            (this as ExtensionAware).configure<EncodingConfiguration> {
                encoding = "UTF-8"
                bomPolicy = WITH_NO_BOM
                properties {
                    encoding = "UTF-8"
                    transparentNativeToAsciiConversion = false
                }
            }
        }
    }
}
