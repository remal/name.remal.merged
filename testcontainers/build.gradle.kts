import name.remal.fromLowerHyphenToLowerCamel
import name.remal.gradle_plugins.dsl.extensions.createFromNotation
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.makeNotTransitive
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension
import name.remal.gradle_plugins.plugins.dependencies.StaticAnalysisTransitiveDependencies
import name.remal.loadServicesList
import name.remal.version.Version
import org.jdom2.Element
import java.util.SortedMap

val dependencyVersions = get(DependencyVersionsExtension::class.java)
val publications = get(PublishingExtension::class.java).publications

val staticAnalysisTransitiveDependencyMatchers = loadServicesList(StaticAnalysisTransitiveDependencies::class.java).asSequence()
    .flatMap { it.dependencyNotations.asSequence() }
    .map {
        if (!it.contains(':')) {
            "$it:*"
        } else {
            it
        }
    }
    .distinct()
    .map(::DependencyNotationMatcher)
    .toList()


with(dependencyVersions) {
    allowAllVersionsFor("org.testcontainers:*")
    rejectVersions("org.testcontainers:*:2+")
}
val versionsRedirect = mapOf<Version, Version>(
)
val artifactVersions: SortedMap<Version, DependencyNotation> = sortedMapOf<Version, DependencyNotation>().apply {
    arrayOf(
        DependencyNotation("org.testcontainers", "testcontainers-bom")
    ).forEach { notation ->
        dependencyVersions.resolveAllVersions(notation)
            .mapNotNull(Version::parseOrNull)
            .forEach { version ->
                get(version)?.let { throw GradleException("Main version $version has been already mapped to $it") }
                val canonicalVersion = versionsRedirect[version] ?: version
                put(version, notation.withVersion(canonicalVersion.toString()).withExtension("pom"))
            }
    }
}
artifactVersions.forEach version@{ artifactVersion, pomNotation ->

    val pomManagedDependencies: Set<DependencyNotation> by lazy {
        configurations.detachedConfiguration(dependencies.createFromNotation(pomNotation))
            .makeNotTransitive()
            .files
            .single()
            .parsePomDependencies()
            .managed
    }

    val allNotations: Set<DependencyNotation> by lazy {
        val configuration = project.configurations.detachedConfiguration(
            *pomManagedDependencies.map { project.dependencies.createFromNotation(it) }.toTypedArray()
        )

        configuration.incoming.resolutionResult.allComponents.asSequence()
            .mapNotNull { it.id as? ModuleComponentIdentifier }
            .map(ModuleComponentIdentifier::notation)
            .filter { it.group == pomNotation.group }
            .toSet()
    }

    val allPublication = publications.create<MavenPublication>("all-$artifactVersion") publication@{
        artifactId = "testcontainers-all"
        version = artifactVersion.toString()

        pom {
            packaging = "jar"
            name.set(provider { pomManagedDependencies.joinToString(" + ") })
        }

        mergedArtifact(
            project = project,
            notations = allNotations,
            automaticModuleName = "testcontainers.all"
        )

        mergedSourcesArtifact(
            project = project,
            notations = allNotations
        )
    }

    (allNotations + pomNotation).forEach { notation ->
        publications.create<MavenPublication>("${notation.module.fromLowerHyphenToLowerCamel()}-$artifactVersion") publication@{
            artifactId = notation.module
            version = artifactVersion.toString()

            pom {
                packaging = "pom"
                name.set(notation.toString())

                withJdomDocument {
                    rootElement.children.filter { it.name == "dependencyManagement" }.forEach(Element::detach)
                    rootElement.children.filter { it.name == "dependencies" }.forEach(Element::detach)

                    val pomDependencies = configurations.detachedConfiguration(
                        dependencies.createFromNotation(notation.withoutClassifier().withExtension("pom"))
                    )
                        .makeNotTransitive()
                        .files
                        .single()
                        .parsePomDependencies()

                    fun Iterable<DependencyNotation>.processNotations() = this
                        .filter { notation -> staticAnalysisTransitiveDependencyMatchers.none { it.matches(notation) } }
                        .map {
                            if (it.group == pomNotation.group) {
                                DependencyNotation(
                                    group = allPublication.groupId,
                                    module = it.module,
                                    version = it.version,
                                    classifier = it.classifier,
                                    extension = it.extension
                                )
                            } else {
                                it
                            }
                        }

                    rootElement.addContent(Element("dependencyManagement", rootElement.namespace).apply dependencyManagement@{
                        addContent(Element("dependencies", rootElement.namespace).apply dependencies@{
                            pomDependencies.managed.processNotations().forEach { dependency(it, rootElement.namespace) }
                        })
                    })

                    rootElement.addContent(Element("dependencies", rootElement.namespace).apply dependencies@{
                        if (pomDependencies.packaging != "pom") {
                            dependency(allPublication.notation, rootElement.namespace)
                        }

                        pomDependencies.direct.processNotations().forEach { dependency(it, rootElement.namespace) }
                    })
                }
            }
        }
    }

}
