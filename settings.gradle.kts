apply { from("gradle/settings.gradle.kts") }

include(
    ":javax-annotation",
    ":testcontainers"
)
