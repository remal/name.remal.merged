import org.gradle.api.JavaVersion.VERSION_12
import org.gradle.api.JavaVersion.VERSION_1_8

allprojects {
    group = "name.remal.merged" + path.replace(':', '.').trimEnd('.')
    version = "0-SNAPSHOT"

    pluginManager.apply("name.remal.gitlab-ci")
    pluginManager.apply("name.remal.default-plugins")
    pluginManager.apply("name.remal.disable-tests")

    pluginManager.withPlugin("java") {
        configure<JavaPluginConvention> {
            sourceCompatibility = VERSION_12
            targetCompatibility = VERSION_1_8
        }

        repositories {
            mavenCentral()
        }
    }

    pluginManager.withPlugin("kotlin") {
        pluginManager.apply("kotlin-allopen")
    }
}
