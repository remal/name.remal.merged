pluginManagement {
    repositories {
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
}

gradle.allprojects {
    buildscript {
        repositories {
            jcenter()
            mavenCentral()
            gradlePluginPortal()
        }

        dependencies {
            classpath("name.remal:gradle-plugins")
        }

        configurations.all {
            dependencies.all {
                if (this is ModuleDependency) {
                    exclude(group = "org.slf4j")
                    exclude(group = "ch.qos.logback")
                    exclude(group = "org.apache.logging.log4j")
                    exclude(group = "log4j")
                    exclude(group = "commons-logging")
                    exclude(group = "org.springframework", module = "spring-jcl")
                    exclude(group = "org.codehaus.groovy")
                    exclude(group = "ant", module = "ant")
                    exclude(group = "org.apache.ant", module = "ant")
                    exclude(group = "org.apache.ant", module = "ant-launcher")
                }
            }

            resolutionStrategy {
                eachDependency {
                    if (target.version.isNullOrEmpty()) {
                        useVersion("+")
                    }
                }

                if (System.getenv("GITLAB_CI") != "true" && System.getenv("CI") != "true") {
                    cacheDynamicVersionsFor(6, "hours")
                    cacheChangingModulesFor(6, "hours")
                }
            }
        }
    }

    configurations.all {
        resolutionStrategy {
            eachDependency {
                if (target.version.isNullOrEmpty()) {
                    useVersion("+")
                }
            }

            if (System.getenv("GITLAB_CI") != "true" && System.getenv("CI") != "true") {
                cacheDynamicVersionsFor(6, "hours")
                cacheChangingModulesFor(6, "hours")
            }
        }
    }
}
